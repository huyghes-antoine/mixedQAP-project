# MixedQAP project - Optimisation

## Authors: 
  Sébastien Verel - Théo Pecqueux - Antoine Huyghes

---
## Description:

The project description is available [here](https://www.overleaf.com/read/phjcbfrmfycr) 

---

## Instance files:

The directory instances/ contains the set of mixed-variable Quadratic Assignment Problem (mixed-QAP) instance files.

---

## Code:

The directory code/src/cpp contains the c++ code. The evaluation function is in the directory code/cpp/src/evaluation, and basic tests are in the directory code/cpp/src/tests,

---

### To compile the code:
In C++:

```cpp
cd code/cpp/src
mkdir build
cd build
mkdir tests
cd tests
cmake ../../tests
make
```
```cpp
cd code/cpp/src
mkdir build
cd build
mkdir exe
cd exe
cmake ../../exe
make
```
---
### To execute the tests:

In C++

```cpp
./t-solution % Pour tester une solution
./t-eval % Pour tester une évaluation
```
---


### Executions 

Run this command if you want to execute a particular algorithm.

```bash
./algoName ../../../../../instances/nameOfTheInstance.dat seed timeInSeconds
```

## Random Search 
```bash
./randomSearch ../../../../../instances/mixedQAP_uni_100_-100_100_1.dat 1 3
```

## HillClimber
```bash
./hillClimbing ../../../../../instances/mixedQAP_uni_100_-100_100_1.dat 1 3
```

## Iterated Local Search 
```bash
./iteratedLocalSearch ../../../../../instances/mixedQAP_uni_100_-100_100_1.dat 1 3
```

## Evolutionary Strategy
```bash
./evolutionStrategy ../../../../../instances/mixedQAP_uni_100_-100_100_1.dat 1 3
```
____
### Answers

All the answers of the project can be found in the [report](rapport/document.pdf)