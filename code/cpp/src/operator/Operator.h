#ifndef _operator_h
#define _operator_h

#include <solution.h>

class Operator {
    public:
        virtual void operator()(Solution & _solution)=0;
};

#endif