#ifndef _randomPermutation_h
#define _randomPermutation_h

#include <Operator.h>
#include <solution.h>
#include <random>
#include <algorithm>


class RandomPermutation : public Operator {
    protected:
        unsigned int n;
        std::default_random_engine & rng;
    public:
        RandomPermutation(std::default_random_engine _rng, unsigned int _n): rng(_rng), n(_n){}

        virtual void operator()(Solution &_solution)
        {
            _solution.sigma.resize(n);
            for( unsigned int k = 0; k < n; k++)
                _solution.sigma[k] = k;

            std::shuffle(std::begin(_solution.sigma), std::end(_solution.sigma), rng);
        }

};

#endif
