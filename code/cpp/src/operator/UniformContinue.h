#ifndef _uniformcontinue_h
#define _uniformcontinue_h

#include<solution.h>
#include<Operator.h>

class UniformContinue: public Operator {
    public: 
        UniformContinue(unsigned int _n):n(_n){  }

        void operator()(Solution & solution){
            solution.x.resize(n);

            for(unsigned i=0; i<n; i++)
                solution.x[i] = 1.0/n;
            solution.modifiedX = true;
        }
    protected: 
        unsigned int n;
};

#endif