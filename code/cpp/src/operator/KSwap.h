#ifndef _KSwap_h
#define _KSwap_h

#include "Operator.h"
#include "solution.h"
#include <random>
#include <algorithm>
#include <RandomPermutation.h>

class KSwap : public Operator {
    public:
        KSwap(std::default_random_engine & _rng, unsigned int _n) : rng(_rng), n(_n) { }

        void operator()(Solution & solution) {
            std::uniform_int_distribution<int> uniformDistribution1(1, (n-1)/2);
            int kmax = uniformDistribution1(rng);
            std::uniform_int_distribution<int> uniformDistribution2(0, kmax-1);
            int kmin = uniformDistribution2(rng);
            RandomPermutation init(rng, solution.sigma.size());
            for (int i=kmin; i<kmax; i++)
                init(solution);
        }
    protected:
        unsigned int n;
        std::default_random_engine & rng;
};

#endif