#ifndef _neighborhoodeval_h
#define _neighborhoodeval_h

#include <random>
#include "mixedQAPeval.h"
#include "search.h"
#include "RandomPermutation.h"
#include "UniformContinue.h"

class NeighborhoodEval {
    public:
        virtual void init(Solution & _solution, std::vector<std::vector<double>>&delta) = 0;

        virtual void update (Solution & _solution, std::pair<unsigned int, unsigned int>& neighbor, std::vector<std::vector<double>>&delta) =0;
};

#endif