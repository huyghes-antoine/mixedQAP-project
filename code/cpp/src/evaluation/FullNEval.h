#ifndef _fullneval_h
#define _fullneval_h

#include <mixedQAPeval.h>
#include <NeighborhoodEval.h>

class FullNEval : public NeighborhoodEval{
    protected:
        MixedQAPeval & eval;
    public:
        FullNEval(MixedQAPeval & _eval): eval(_eval){ }

        virtual void init(Solution & _solution, std::vector< std::vector<double> > & delta) {
            unsigned int tmp, i, j;

            double currentFitness = _solution.fitness;

            for ( i = 1; i < _solution.sigma.size(); i++){
                for ( j = 0; j < i; j++){
                    tmp = _solution.sigma[i];
                    _solution.sigma[i] = _solution.sigma[j];
                    _solution.sigma[j] = tmp;

                    eval(_solution);
                    delta[i][j] = _solution.fitness - currentFitness;

                    _solution.sigma[j] = _solution.sigma[i];
                    _solution.sigma[i] = tmp;
                }
            }

            _solution.fitness = currentFitness;
        }

        virtual void update(Solution & _solution, std::pair<unsigned int, unsigned int> & neighbor, std::vector< std::vector<double> > & delta){
            //delta[neighbor.first][neighbor.second] = _solution.fitness - delta[neighbor.first][neighbor.second];
            init(_solution, delta);
        }
};
#endif