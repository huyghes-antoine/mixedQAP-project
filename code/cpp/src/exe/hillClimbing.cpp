/*

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
  version 0 : 2020/12/09

    Mixed vairable QAP
    

To compile:

mkdir build
cd build
cmake ../src/exe
make

To execute (in build):

./hillClimbing ../../../../../instances/mixedQAP_uni_6_1_100_1.dat  1 3

*/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <random>

#include "solution.h"
#include "mixedQAPeval.h"
#include "RandomPermutation.h"
#include "FullNEval.h"
#include "HillClimber.h"
#include "UniformContinue.h"

using namespace std;

/**
 *  Main function
 */
int main(int argc, char **argv) {
    //---------------------------------
    // *** Arguments

    // file instance name
    char * fileName = argv[1];

    // random seed
    unsigned int seed = atoi(argv[2]);

    // time stopping criterium (in second)
    unsigned int duration = atoi(argv[3]);

    //---------------------------------

    auto rng = std::default_random_engine {};
    rng.seed(seed);    


    // evaluation function
    MixedQAPeval eval(fileName);
    FullNEval neighbor(eval);
    // random init
    RandomPermutation init(rng, eval.n);

    // Uniform initial of the contiunous variable
    UniformContinue uniform(eval.n);

    // HC
    HillClimber search(eval, neighbor,  eval.n);

    // precision print


    //---------------------------------

    Solution solution(eval.n);

    init(solution);
    uniform(solution);

    eval(solution);
    std::cout.precision(15);
    search.timeLimit(time(NULL) + duration);

    cout << "Fitness avant Hill-Climber : " << solution << endl;
    
    search(solution);
    
    cout << "Fitness avant Hill-Climber : " << solution << endl;

    //----------------------------------
    // ok

    return 1;
}
