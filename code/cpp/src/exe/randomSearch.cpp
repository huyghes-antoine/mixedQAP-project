/*

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
  version 0 : 2020/12/09

    Mixed vairable QAP
    

To compile:

mkdir build
cd build
cmake ../src/exe
make

To execute (in build):

./randomSearch ../../../instances/mixedQAP_uni_6_1_100_1.dat  1 3

*/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <random>

#include <solution.h>
#include <mixedQAPeval.h>
#include <RandomPermutation.h>
#include <RandomSearch.h>
#include <UniformContinue.h>

using namespace std;

/**
 *  Main function
 */
int main(int argc, char **argv) {
    //---------------------------------
    // *** Arguments

    // file instance name
    char * fileName = argv[1];

    // random seed
    unsigned int seed = atoi(argv[2]);

    // time stopping criterium (in second)
    unsigned int duration = atoi(argv[3]);

    //---------------------------------

    auto rng = std::default_random_engine {};

    rng.seed(seed);    

    // evaluation function
    MixedQAPeval eval(fileName);

    // random init
    RandomPermutation init(rng, eval.n);

    // Uniform initial of the contiunous variable
    UniformContinue uniform(eval.n);

    // HC
    RandomSearch search(rng, eval);

    // precision print
    std::cout.precision(15);

    //---------------------------------

    Solution solution(eval.n);

    init(solution);
    uniform(solution);

    eval(solution);

    search.timeLimit(time(NULL) + duration);

    search(solution);

    cout << solution << endl;

    //----------------------------------
    // ok

    return 1;
}
