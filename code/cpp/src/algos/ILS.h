#ifndef _ILS_h
#define _ILS_h_

#include <random>
#include "mixedQAPeval.h"
#include "search.h"
#include "HillClimber.h"
#include "KSwap.h"

class ILS : public Search {
public:
    ILS(MixedQAPeval & _eval,NeighborhoodEval & _neighbohood, unsigned _n, KSwap & _kSwap):eval(_eval), neighborhoodEval(_neighbohood), n(_n), kSwap(_kSwap){
    }

    virtual void operator()(Solution & _solution){
        HillClimber hillClimber(eval, neighborhoodEval, eval.n);
        hillClimber.timeLimit(time(NULL) + 1);
        hillClimber(_solution);
        while(time(NULL) < timeLimit_){
            Solution &save = _solution;
            kSwap(save);
            
            hillClimber.timeLimit(time(nullptr) + 1);
            hillClimber(save);

            if(save.fitness < _solution.fitness){
                _solution = save;
                eval(_solution);
            }

        }

    }
        NeighborhoodEval & neighborhoodEval;
        MixedQAPeval eval;
        unsigned n;
        KSwap kSwap;
};

#endif