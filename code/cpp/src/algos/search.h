#ifndef _search_h
#define _search_h

#include <iostream>
#include <fstream>
#include <vector>

#include "solution.h"

class Search {
    public:
        Search(){}

        virtual void operator()(Solution & _solution) = 0;
        virtual void timeLimit(time_t limit) {
            timeLimit_ = limit;
        }
    protected:
        time_t timeLimit_;
};

#endif