#ifndef _hillclimber_h
#define _hillclimber_h_

#include <random>
#include <math.h>
#include "FullNEval.h"
#include "mixedQAPeval.h"
#include "search.h"
#include "NeighborhoodEval.h"

class ES : public Search {
public:
    ES(MixedQAPeval & _eval, unsigned _n, std::default_random_engine & _rng):eval(_eval), n(_n), rng(_rng){}

    virtual void operator()(Solution & _solution){
        double sigma = 1;
        double gamma = 1.2;
        int nb_ite = 0;
        int cpt = 0;
        double lastFitness = 0;
        std::uniform_real_distribution<double> uniDist(-1.0, 1.0);
        Solution cpSolution(_solution);
        while (nb_ite < 200){
            std::cout << cpt << " : " << cpSolution.fitness << " " << _solution.fitness<< " -> "<< std::abs(_solution.fitness - lastFitness) << " " << lastFitness <<" " << nb_ite << " " << sigma << std::endl;
            //application des modification a la copie de solution
            for(int i = 0; i < _solution.x.size(); i++){
                std::cout << cpSolution.x.at(i) << " ";
                cpSolution.x.at(i) +=  sigma * uniDist(rng);
                std::cout << cpSolution.x.at(i) << " ";
            }
            std::cout << std::endl;
            
            //méthode de réparation
            for(int i = 1; i < cpSolution.x.size(); i++)
                if(cpSolution.x.at(i) < 0)
                    cpSolution.x.at(i) = -cpSolution.x.at(i);

            int sum = 0;
            for(int i = 0; i<cpSolution.x.size(); i++)
                sum += cpSolution.x.at(i);
            if(sum == 0)
                for(int i = 0; i < cpSolution.x.size(); i++)
                    cpSolution.x.at(i) = (1/cpSolution.x.size());
            else if(sum != 1)
                for(int i = 0; i < cpSolution.x.size(); i++)
                    cpSolution.x.at(i) = cpSolution.x.at(i)/sum;

            if(cpt == 0)
                lastFitness = _solution.fitness;
            cpSolution.modifiedX = true;
            eval(_solution);
            eval(cpSolution);
            //vérification de la meilleur solution
            if(cpSolution.fitness < _solution.fitness){
                _solution = cpSolution;
                sigma = sigma * gamma;
                lastFitness = _solution.fitness;
            }else{
                sigma = sigma * std::pow(gamma, 1/5);
            }

            //vérification de la condition d'arret
            if(std::abs(_solution.fitness - lastFitness) >= std::pow(10, -6)){
                nb_ite = 0;
            }
            nb_ite += 1;
            cpt += 1;
            std::cout << cpt << " : " << cpSolution.fitness << " " << _solution.fitness<< " -> "<< std::abs(_solution.fitness - lastFitness) << " " << lastFitness <<" " << nb_ite << " " << sigma << std::endl;
    
            std::cout << "\n" << std::endl;
        }
    }
    protected:
        MixedQAPeval & eval;
        //NeighborhoodEval & neighborhoodEval;
        unsigned n;
        std::default_random_engine & rng;
};

#endif