#!/bin/bash

exeDIR=../code/cpp/src/build/exe
instanceDIR=../instances

Nmin=20
Nmax=100
instanceType="uni"
instanceSeed=1
listBounds="0 1"

nRun=30
timeLimit=1

EXE=randomSearch

output_performance=performance_${EXE}.csv
output_solution=solution_${EXE}.dat

> ${output_solution}
echo instanceType boundsType A B instanceSeed n idRun fitness > ${output_performance}
for boundsT in ${listBounds}
do
	if [ ${boundsT} -eq 0 ] ;
	then
		A=1
		B=100
	else
		A=-100
		B=100
	fi

	for((n=${Nmin};n<=${Nmax};n++))
	do
		instanceName=${instanceDIR}/mixedQAP_${instanceType}_${n}_${A}_${B}_${instanceSeed}.dat

		for((i=1;i<=${nRun};i++))
		do
			echo ${instanceName} ${i}
			${exeDIR}/${EXE} ${instanceName} ${i} ${timeLimit} > tmp

			cat tmp >> ${output_solution}

			echo -n ${instanceType} ${boundsT} ${A} ${B} ${instanceSeed} ${n} ${i}' ' >> ${output_performance}
			awk -- '{ print($1); }' tmp >> ${output_performance}
		done
	done
done

